const express = require('express');
const app = express();
const port = 4000;
const cors = require('cors');
const result = {}

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get('/', (request, response) => {
    response.send("Hello Welcome!")
})
app.get('/result', (request, response) => {
    response.send(result)
})
app.listen(port, () => console.log(`Server is running at port ${port}`));